from cliente import Cliente
from datetime import date

class Fila:
    def __init__(self):
        """
        Instancia e pega a data de hoje
        """
        self.no_princ=None
        self.cont=0 #contador geral
        self.base=date.today().day*10000+date.today().month*1000000 # Base para geração de senha
        
    def adic(self, matricula):
        self.senha=self.base+self.cont
        if self.no_princ is None: #Caso para fila vazia
            self.no_princ = Cliente(self.senha,matricula[0])
            self.cont+=1
            del matricula[0]
            if len(matricula)>0: #Caso o primeiro da fila tenha várias matrículas associadas 
                no_atual = self.no_princ
                for i in matricula:
                    no_atual.prox_no = Cliente(self.base+self.cont,i)
                    no_atual = no_atual.prox_no
                    self.cont+=1
                return
            else:
                    return
        no_atual = self.no_princ
        while no_atual.prox_no is not None:
            no_atual = no_atual.prox_no
        for i in matricula: #Adicionar 1 ou mais usuários
            no_atual.prox_no = Cliente(self.base+self.cont,i)
            no_atual = no_atual.prox_no
            self.cont+=1
            
        
    
    def remove(self,senha):
        if self.no_princ is None:
            return
        no_esq = None
        no_atual = self.no_princ
        if no_atual.senha==senha:
            self.no_princ=no_atual.prox_no
        while True:
            no_esq=no_atual
            no_atual=no_atual.prox_no
            if no_atual is None:
                break
            if no_atual.senha == senha:
                no_esq.prox_no = no_atual.prox_no
                break

    def econtrar(self,senha):
        """
        Eu tinha criado essa função antes de alterar a idea, mas deixa aí porque
        deu trabalho pra fazer.
        """
        if self.no_princ is None:
            print('A fila está vazia.')
            return None
        no_atual=self.no_princ
        while no_atual is not None:
            if no_atual.prox_no.senha == senha:
                return no_atual
            no_atual=no_atual.prox_no
        return no_atual
        
    def mostrarFila(self):
        #senhas = []
        no_atual = self.no_princ
        print('Lista de alunos que reservaram senha:')
        while no_atual is not None:
            print('Senha:',no_atual.senha,'| Matrícula: ',no_atual.matricula)
            no_atual = no_atual.prox_no
        #print("LinkedList: {} ".format(senhas))

